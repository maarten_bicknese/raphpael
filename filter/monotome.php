<?php

namespace Raphpael\Filter;

/**
 * Only allows one color (and a background color, don't tell the others)
 * 
 * Default is black and white
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Monotome implements FilterInterface
{
    /**
     * Determines wether a vector should be foreground or background
     * @var int
     */
    public $treshold = 127;
    
    /**
     * Color information for the background
     * @var array
     */
    private $background = array('red' => 255, 'green' => 255, 'blue' => 255);
    
    /**
     * Color information for the foreground
     * @var array
     */
    private $foreground = array('red' => 0, 'green' => 0, 'blue' => 0);
    
    /**
     * Modifies given vector
     * @param  array $vector Unmodified vector information
     * @return array Modified vector information
     */
    public function filter($vector)
    {
        $color_avg = ($vector[2]['red'] + $vector[2]['green'] + $vector[2]['blue']) / 3;
        $vector[2] = $color_avg > $this->treshold ? $this->background : $this->foreground;
        return $vector;
    }
    
    /**
     * Set background color to given RGB values
     * @param int $red   Value from 0 to 256
     * @param int $green Value from 0 to 256
     * @param int $blue  Value from 0 to 256
     */
    public function setBackground($red, $green, $blue)
    {
        $this->background = array('red' => $red, 'green' => $green, 'blue' => $blue);
    }
    
    /**
     * Set foreground color to given RGB values
     * @param int $red   Value from 0 to 256
     * @param int $green Value from 0 to 256
     * @param int $blue  Value from 0 to 256
     */
    public function setForeground($red, $green, $blue)
    {
        $this->foreground = array('red' => $red, 'green' => $green, 'blue' => $blue);
    }
    
}

// EOF