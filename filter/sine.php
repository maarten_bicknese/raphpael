<?php

namespace Raphpael\Filter;

/**
 * Displace 'pixels' based on sine wave
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Sine implements FilterInterface
{
    
    /**
     * Which axis to transform
     * 
     * 0 = X-axis
     * 1 = Y-axis
     * 
     * @var int
     */
    protected $axis = 0;
    
    /**
     * How far should the vectors be displaced
     * @var int
     */
    protected $magnitude = 1;
    
    /**
     * Modifies given vector
     * @param  array $vector Unmodified vector information
     * @return array Modified vector information
     */
    public function filter($vector)
    {
        $vector[$this->axis] += round(sin($vector[($this->axis + 1) % 2])) * $this->magnitude;
        return $vector;
    }
    
    /**
     * Change the magnitude
     * @param int $magnitude
     */
    public function setMagnitude($magnitude)
    {
        $this->magnitude = round($magnitude);
    }
    
    /**
     * Set the axis to transform to the X-axis
     */
    public function transformX()
    {
        $this->axis = 0;
    }
    
    /**
     * Set the axis to transform to the Y-axis
     */
    public function transformY()
    {
        $this->axis = 1;
    }
    
}

// EOF