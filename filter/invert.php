<?php

namespace Raphpael\Filter;

/**
 * Invert all colors
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Invert implements FilterInterface
{
    
    /**
     * Modifies given vector
     * @param  array $vector Unmodified vector information
     * @return array Modified vector information
     */
    public function filter($vector)
    {
        $vector[2] = array(
            'red' => (255-$vector[2]['red']),
            'green' => (255-$vector[2]['green']),
            'blue' => (255-$vector[2]['blue']),
        );
        return $vector;
    }
    
}

// EOF