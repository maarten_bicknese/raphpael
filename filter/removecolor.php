<?php

namespace Raphpael\Filter;

/**
 * Removes set colors
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class RemoveColor implements FilterInterface
{
    /**
     * Array of colors to filter from the picture (values can be red, green and blue)
     * @var array
     */
    protected $colors = array();
    
    /**
     * Modifies given vector
     * @param  array $vector Unmodified vector information
     * @return array Modified vector information
     */
    public function filter($vector)
    {
        foreach ($this->colors as $color)
        {
            $vector[2][$color] = 0;
        }
        return $vector;
    }
    
    /**
     * Adds or removes (toggle) the color from the color array
     * @param string $color Red, green or blue
     */
    protected function toggle($color)
    {
        $key = array_search($color, $this->colors);
        
        if ($key === false) {
            $this->colors[] = $color;
        } else {
            array_splice($this->colors, $key, 1);
        }
    }
    
    /**
     * Toggles the blue filter
     * @return void
     */
    public function toggleBlue()
    {
        return $this->toggle('blue');
    }
    
    /**
     * Toggles the green filter
     * @return void
     */
    public function toggleGreen()
    {
        return $this->toggle('green');
    }
    
    /**
     * Toggles the red filter
     * @return void
     */
    public function toggleRed()
    {
        return $this->toggle('red');
    }
    
}

// EOF