<?php

namespace Raphpael\Filter;

/**
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
interface FilterInterface
{
    
    public function filter($vector);
    
}

// EOF