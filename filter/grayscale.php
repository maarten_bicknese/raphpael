<?php

namespace Raphpael\Filter;

/**
 * Turns all colors into their grayscale equivalent
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Grayscale implements FilterInterface
{
    
    /**
     * Modifies given vector
     * @param  array $vector Unmodified vector information
     * @return array Modified vector information
     */
    public function filter($vector)
    {
        $color_avg = round(($vector[2]['red'] + $vector[2]['green'] + $vector[2]['blue']) / 3);
        $vector[2]['red'] = $vector[2]['green'] = $vector[2]['blue'] = $color_avg;
        return $vector;
    }
    
}

// EOF