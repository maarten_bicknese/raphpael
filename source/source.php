<?php

namespace Raphpael\Source;

/**
 * OOP-ify PHPs image manipulation tools
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Source
{
    
    /**
     * Path to the original image file
     * @var string
     */
    protected $filename;
    
    /**
     * Mime-type of the original image file
     * @var string
     */
    protected $mime;
    
    /**
     * Image Resource as created by imagecreatefrom
     * @var resource
     */
    protected $resource;
    
    /**
     * Color representing the alpha color of the image
     * @var int
     */
    protected $color_alpha;
    
    /**
     * Width of the image
     * @var int
     */
    protected $width  = 0;
    /**
     * Public access to the width property
     * @return int
     */
    public function width()  { return $this->width; }
    /**
     * Height of the image
     * @var int
     */
    protected $height = 0;
    /**
     * Public access to the height property
     * @return int
     */
    public function height() { return $this->height; }
    
    /**
     * Creates the source object based on given existing file
     * @param string Path to the file to be converted
     */
    public function __construct($filename)
    {
        $this->init($filename);
    }
    
    /**
     * Returns the color at the given coördinates
     * @param  int   $x X-coördinate
     * @param  int   $y Y-coördinate
     * @return array Color at given coördinates or false when there is no color
     */
    public function colorAt($x, $y)
    {
        $color = @imagecolorat($this->resource, $x, $y);
        if ($color == $this->color_alpha) { return false; }
        
        return imagecolorsforindex($this->resource, $color);
    }
    
    /**
     * (Re)initialize the source object based on given file
     * @param string $filename Path to the file to be converted
     */
    public function init($filename)
    {
        $this->filename    = $filename;
        $this->resource    = imagecreatefromgif($filename);
        $info              = getimagesize($filename);
        $this->mime        = $info['mime'];
        $this->width       = $info[0];
        $this->height      = $info[1];
        $this->color_alpha = imagecolortransparent($this->resource);
    }
    
}

// EOF