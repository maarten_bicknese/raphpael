<?php

namespace Raphpael;

use Raphpael\Source\Source;
use Raphpael\Output\OutputAbstract;

/**
 * This is the main object to turn a pixel based image to a vector based image.
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Image
{
    /**
     * The image source object
     * @var Source
     */
    protected $source;
    
    /**
     * Single depth array of vectors corresponding to the pixels
     * @var array
     */
    protected $vectors;
    
    /**
     * Render engine to turn the vector array into something useful
     * @var OutputAbstract
     */
    public $renderer;
    
    /**
     * Create a new image object
     * @param string $filename Path to image to base object upon
     */
    public function __construct($filename)
    {
        $this->source = new Source($filename);
    }
    
    /**
     * Populates vector array based on source pixel information
     */
    public function calculateVectors()
    {   
        $this->vectors = array();
        for ($x = 0; $x < $this->source->width(); ++$x) {
            for ($y = 0; $y < $this->source->height(); ++$y) {
                if (false !== ($color = $this->source->colorAt($x, $y))) {
                    $this->vectors[] = array($x, $y, $color);
                }
            }
        }
    }
    
    /**
     * Use the set render engine to turn pixel information into a string
     * 
     * Vectors will be calculated if this was not done manualy.
     * If no render engine is set, the SVG render engine will be used by default
     * 
     * @return string The rendered vectors
     */
    public function render()
    {
        if (empty($this->vectors)) { $this->calculateVectors(); }
        if (!$this->renderer) { $this->renderer = new Output\Svg(); }
        
        return $this->renderer->render($this->vectors, $this->source->width(), $this->source->height());

    }
    
}

// EOF