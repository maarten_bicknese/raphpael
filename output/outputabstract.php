<?php

namespace Raphpael\Output;

use Raphpael\Filter\FilterInterface;

/**
 * Abstract interface for output classes
 * 
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
abstract class OutputAbstract
{
    
    /**
     * Array of filters to apply. The order is important!
     * @var array
     */
    protected $filters = array();
    
    /**
     * Adds a new filter to the array
     * @param FilterInterface $filter
     */
    public function addFilter($filter)
    {
        $this->filters[] = $filter;
    }
    
    /**
     * Clears the filter array
     */
    public function clearFilters()
    {
        $this->filters = array();
    }
    
    /**
     * Applies all filters in the filter array to the given vector
     * @param  array $vector Unmodified vector information
     * @return array Vector information modified by all set filters
     */
    public function filter($vector)
    {
        foreach ($this->filters as $filter) {
            $vector = $filter->filter($vector);
        }
        return $vector;
    }
    
    public abstract function render($vectors, $width, $height);
    
    /**
     * Removes first occurance of the given filter
     * @param FilterInterface $filter
     */
    public function removeFilter($filter)
    {
        array_splice($this->filters, array_search($filter, $this->filters), 1);
    }
   
}

// EOF