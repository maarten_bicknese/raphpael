<?php

namespace Raphpael\Output;

use Raphpael\Output\OutputAbstract;

/**
 * Renders a set of vectors into SVG format
 *
 * @author Maarten Bicknese <Maarten@Bicknese.nl>
 * @package Raphpael
 */
class Svg extends OutputAbstract
{
    
    /**
     * Renders all vectors as SVG rectangles
     * @param array   $vectors array of vectors to be rendered
     * @param int     $width   Width of the final picture
     * @param int     $height  Height of the final picture
     * @return string The rendered vectors
     */
    public function render($vectors, $width = 0, $height = 0)
    {
        $output = '<?xml version="1.0"?>
                <svg width="100%" height="100%" 
                viewBox="0 0 ' . $width . ' ' . $height . '" version="1.1"
                xmlns="http://www.w3.org/2000/svg">';
        
        foreach ($vectors as $vector) {
            $vector = $this->filter($vector);
            $output .= '<rect shape-rendering="crispEdges"'
                     . " x=\"{$vector[0]}\"" 
                     . " y=\"{$vector[1]}\"" 
                     . '" width="1" height="1" fill="rgb(' . "{$vector[2]['red']},{$vector[2]['green']},{$vector[2]['blue']}" . ')"></rect>';
        }

        $output .= '</svg>';
        return $output;
    }
    
}

// EOF